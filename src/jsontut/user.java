/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsontut;

/**
 *
 * @author saad
 */
public class user {
    
    private String name;
    private String Company ;
    private long Salary ;

    
    public user() {
    }

    public user(String name, String Company, long Salary) {
        this.name = name;
        this.Company = Company;
        this.Salary = Salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String Company) {
        this.Company = Company;
    }

    public long getSalary() {
        return Salary;
    }

    public void setSalary(long Salary) {
        this.Salary = Salary;
    }
    
    
    
}
