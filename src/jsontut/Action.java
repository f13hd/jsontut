/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsontut;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author saad
 */
public class Action {

    FileWriter writer;
    File file = new File("JSON3.json");
    ArrayList list = new ArrayList();

    public Action() {

        try {

            if (file.exists()) {

                writer = new FileWriter(file, true);

            } else {

                writer = new FileWriter(file, true);

            }
            LoadData();
            System.out.println("App is Ready for using ....");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    public void AddNew(user u) throws IOException {

        JSONObject jsonObj = new JSONObject();
        jsonObj.put("name", u.getName());
        jsonObj.put("Company", u.getCompany());
        jsonObj.put("Salary", u.getSalary());

        writer.write(jsonObj.toJSONString() + "\n");

        writer.flush();

    }

    public user readUser() throws IOException, ParseException {

        FileReader reader = new FileReader(file);

        JSONParser p = new JSONParser();

        Object o = p.parse(reader);

        JSONObject obj = (JSONObject) o;
        String name = (String) obj.get("name");
        String company = (String) obj.get("Company");
        long salary = (long) obj.get("Salary");

        user u = new user(name, company, salary);

        return u;

    }


    public void printAll() throws IOException, ParseException {

        FileReader reader = new FileReader(file);
        BufferedReader buf = new BufferedReader(reader);
        String line;
        JSONParser parser = new JSONParser();

        Object obj;
        String name, Company;
        long Salary;

        if ((line = buf.readLine()) == null) {
            System.out.println("The File is Empty !!");
        } else {
            while ((line = buf.readLine()) != null) {

                obj = parser.parse(line);
                JSONObject jsonObj = (JSONObject) obj;
                name = (String) jsonObj.get("name");
                Company = (String) jsonObj.get("Company");
                Salary = (long) jsonObj.get("Salary");

                System.out.println(name + " - " + Company + " - " + Salary);
            }
        }
        buf.close();

    }

    public String updateUser(user old, user updated) {
        try {
        JSONObject oldUserObj = new JSONObject();
        JSONObject updateUserObj = new JSONObject();
        
        oldUserObj.put("name", old.getName());
        oldUserObj.put("Company", old.getCompany());
        oldUserObj.put("Salary", old.getSalary());
        
        updateUserObj.put("name", updated.getName());
        updateUserObj.put("Company", updated.getCompany());
        updateUserObj.put("Salary", updated.getSalary());
        
        String oldUserStr =  oldUserObj.toJSONString();
        String updateUserStr = updateUserObj.toJSONString();
        
            ActionUpdateUser(oldUserStr, updateUserStr);
            return "Updated Successfully ...";
        
        }
        catch (Exception pe)
        {
            System.out.println("Error Parsing ... update User");
        }

        return "ERROR UPDATING..";
    }
    
    private void ActionUpdateUser(String old , String updated)
    {
        try{
        BufferedReader reader = new BufferedReader(new FileReader(file));
        File tmpFile = new File(file.getAbsolutePath()+".tmp");
        FileWriter w = new FileWriter(tmpFile);
        
        String line ;
        while ((line = reader.readLine()) != null)
        {
            if (line.equals(old))
            {
                w.write(updated+"\n");
                w.flush();
            }
            else {
                w.write(line+"\n");
                w.flush();
            }
        }
        
        reader.close();
        w.close();
        writer.close();
        
        
        Files.copy(tmpFile.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
       
        
        if (!tmpFile.delete())
        {
            System.out.println("tmp File not deleted...");
        }
        
        }catch (IOException o)
        {
            System.out.println("Error ... ActionUpdateUser");
        }
    }

    public String deleteUser(user u) {
        String lineToRemove = "";
        BufferedReader buf = null;
        String line = "";
        try {
            buf = new BufferedReader(new FileReader(file));
            JSONParser parser = new JSONParser();

            while ((line = buf.readLine()) != null) {
                Object jsonvalue = parser.parse(line);
                JSONObject obj = (JSONObject) jsonvalue;

                if (obj.containsValue(u.getName())) {
                    lineToRemove = line;
                    break;
                }

            }

            buf.close();
            removeLine(lineToRemove);
            return " Removed...";

        } catch (ParseException ex) {
            System.out.println("Error Parsing");
        } catch (IOException ioe) {
            System.out.println("Error Files !!!" + ioe.getMessage());
            ioe.printStackTrace();
        }

        return "Not Removed...";

    }

    private void removeLine(String removeLine) {
        BufferedReader buf = null;
        FileWriter wr = null;
        File tempFile = null;
        try {

            buf = new BufferedReader(new FileReader(file));
            tempFile = new File(file.getAbsolutePath() + ".tmp");

            wr = new FileWriter(tempFile);
            String line = "";

            while ((line = buf.readLine()) != null) {
                
                if (!line.trim().equals(removeLine)) {

                    wr.write(line+"\n");
                    wr.flush();
                }
            }

            buf.close();
            wr.close();
            writer.close();
            Path oldFile = file.toPath();
            Path newFile = tempFile.toPath();
            
            Files.copy(newFile, oldFile, StandardCopyOption.REPLACE_EXISTING);
            tempFile.deleteOnExit();
        } catch (IOException ex) {
            Logger.getLogger(Action.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private ArrayList LoadData() throws FileNotFoundException, IOException, java.util.NoSuchElementException {

        BufferedReader buf = new BufferedReader(new FileReader(file));
        String line;
        while ((line = buf.readLine()) != null) {
            list.add(line);

        }
        buf.close();

        return list;

    }

    public ArrayList getData() throws IOException {
        list.clear();
        list = LoadData();
        return list;
    }

    public user ParseData(Object o) throws ParseException {

        JSONParser parser = new JSONParser();
        Object oo = parser.parse(o.toString());
        JSONObject jsonObj = (JSONObject) oo;

        user u = new user();

        String n = (String) jsonObj.get("name");
        String c = (String) jsonObj.get("Company");
        Long s = (Long) jsonObj.get("Salary");

        u.setCompany(c);
        u.setName(n);
        u.setSalary(s);

        return u;

    }

}
